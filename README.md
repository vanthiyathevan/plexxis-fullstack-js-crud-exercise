# Plexxis Interview Exercise

## Requirements

Create a simple but __impressive__ (looks good, works well, has intuitive design, etc.) CRUD application that can do the following:

1) Retrieve employees from a REST API  
2) Display the employees in a React application  
3) Has UI mechanisms for creating and deleting employees  
4) Has API endpoints for creating and deleting employees  
5) Edit your version of the `README.md` file to explain to us what things you did, where you focussed your effort, etc.

*Read over the `Bonus` objectives and consider tackling those items as well*

## Bonus (Highly Encouraged)

1) Use a relational database to store the data (SQLite, MariaDB, Postgres)  
2) UI mechanisms to edit/update employee data  
3) Add API endpoint to update employee data  
4) Use [React Table](https://react-table.js.org)  

## Getting Started

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). The front-end app runs off localhost:3000. The REST API is located in the /server folder and runs off localhost:8080. The data is being served from a JSON file located in the /server/data folder. Run `npm start` to start both servers.

## Getting it Done

* You are free to use whatever libraries that you want. Be prepared to defend your decisions.
* There is no time limit. Use as little or as much time as is necessary to showcase your abilities.
* You should fork or clone our repository into your own repository.
  * Send us the link when you are done the exercise (pglinker at plexxis dot com).

If you do well on the test, we will bring you in for an interview. Your test results will be used as talking points.  

 __This is your chance to amaze us with your talent!__

> I've re-bootstraped this app to use typescript using `yarn create react-app my-app --typescript`.

## Typescript

* I default to using typescript. It helps with editor auto-completeion and reduce bug related to types.
* The server has it's own `tsconfig.json` file to output the transpiled js files.

## Server

express server and using REST api. Using ts-node, we can run typescript without having to complile every time. Having typings for modules from definately-typed eases the pain of having to remeber the function signatures and modules.

## DB

Using sqlite3 and `sqlite` npm package as it has migration feature to bootstrap DB and support promise.
When the server starts the DB is created or reset and data from  `employees.json` is imported. This gives a fresh start.
All the DB queries are abstracted away in the db module

## Client

* Using `react@next` so I can use hooks. Hooks make a lot of things simpler. As you can see in `Store.ts` a simple redux like reducer is created and using the react context, and state and action are passed as value. A simple custom hook abstract away call to `useContext`.
* The store only manages 2 states. 1 for list of employees and 1 for managing employee view
* Using `react-table` for table view. Table has a column to show colour, and an action column for edit and delete actions
* The pagination is disabled for infinite scrolling, 
* Using `css grid` for layout makes things cleaner and easier
* `material-icons` is imported from `unpkg` as to not polute node_modules
* I've imported and used pure css framework from `unpkg` as it is small and nice looking
* The field editor makes use Formik because it abstract away all input handlers. We can add validation if needed
* The field editor is also responsive thanks for `css grid`
* the `toHexColour` util function converts any color text (hex, rgba and text) to hex as the colour input doesn't like other formats
