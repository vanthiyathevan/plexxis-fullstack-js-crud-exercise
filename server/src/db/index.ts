import { join } from 'path';
import { readFile, read } from 'fs';
import { promisify } from 'util';
import { open } from 'sqlite';
import { Employee } from '../../../types';

const aReadFile = promisify(readFile);

let dbPromise = open(join(__dirname, './plexxis.sqlite'));

export const migrate = async () => {
  const db = await dbPromise;
  await db.migrate({ migrationsPath: join(__dirname, '../../migrations'), force: 'last' });
  await db.run('DELETE FROM Employee');
  const json = await aReadFile(join(__dirname, '../../data/employees.json'), { encoding: 'utf-8' });
  const values = (JSON.parse(json) as Employee[])
    .reduce<string[]>((agg, e) => [
      ...agg, `(${e.id}, "${e.name}", "${e.code}", "${e.profession}", "${e.color}", "${e.city}", "${e.branch}", ${e.assigned ? 1 : 0})`
    ], []);
  await db.run(`
    INSERT INTO Employee (id, name, code, profession, color, city, branch, assigned)
    VALUES ${values.join(', ')}
  `);
};

export const getEmployees = async (): Promise<Employee[]> => {
  const db = await dbPromise;
  const all = db.all(`SELECT * FROM Employee`);
  return all;
};

export const deleteEmployee = async (id: number) => {
  const db = await dbPromise;
  await db.run('DELETE FROM Employee WHERE id = ?', [id]);
};

export const insertEmployee = async (e: Omit<Employee, 'id'>): Promise<Employee> => {
  const db = await dbPromise;
  const { lastID } = await db.run(`
    INSERT INTO Employee (name, code, profession, color, city, branch, assigned)
    VALUES ($name, $code, $profession, $color, $city, $branch, $assigned)
  `, {
      $code: e.code,
      $profession: e.profession,
      $color: e.color,
      $city: e.city,
      $branch: e.branch,
      $assigned: e.assigned ? 1 : 0
    });
  return { id: lastID, ...e } //await db.get(`SELECT * FROM Employee WHERE id = ?`, [lastID]);
};

export const updateEmployee = async (e: Employee): Promise<void> => {
  const db = await dbPromise;
  await db.run(`
    UPDATE Employee
    SET name = IFNULL($name, name),
      code = IFNULL($code, code),
      profession = IFNULL($profession, profession),
      color = IFNULL($color, color),
      city = IFNULL($city, city),
      branch = IFNULL($branch, branch),
      assigned = IFNULL($assigned, assigned)
    WHERE id = $id
  `, {
      $id: e.id,
      $code: e.code,
      $profession: e.profession,
      $color: e.color,
      $city: e.city,
      $branch: e.branch,
      $assigned: e.assigned ? 1 : 0
    });
};

