import express from 'express';
import bodyParser from 'body-parser';
import createError from 'http-errors';
import cors, { CorsOptions } from 'cors';
import { migrate, getEmployees, insertEmployee, updateEmployee, deleteEmployee } from './db';

const corsOptions: CorsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
};

(async () => {
  await migrate();

  const app = express();
  app
    .use(cors(corsOptions))
    .use(bodyParser.json())
    .get('/api/employees', async (_, res, next) => {
      console.log("GET", '/api/employees');
      try {
        const emp = await getEmployees();
        res.status(200);
        res.json(emp);
      } catch (err) {
        next(createError(500, err));
      }
    })
    .put('/api/employees/:id', async (req, res, next) => {
      console.log('PUT', '/api/employees');
      try {
        await updateEmployee(req.body);
        res.status(200);
        res.end();
      } catch (error) {
        next(createError(500, error));
      }
    })
    .post('/api/employees', async (req, res, next) => {
      console.log('POST', '/api/employees');
      try {
        const newEmp = await insertEmployee(req.body);
        res.status(200);
        res.json(newEmp);
      } catch (error) {
        next(createError(500, error));
      }
    })
    .delete('/api/employees/:id', async (req, res, next) => {
      console.log('DELETE', '/api/employees/:id');
      try {
        const { id } = req.params;
        await deleteEmployee(id);
        res.status(200);
        res.end();
      } catch (error) {
        next(createError(500, error));
      }
    });

  app.listen(3001, () => console.log('Job Dispatch API running on port 3001!'));
})();
