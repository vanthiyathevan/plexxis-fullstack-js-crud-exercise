
declare global {
  export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
}

export type Employee = {
  id: number;
  name: string;
  code: string;
  profession: string;
  color: string;
  city: string;
  branch: string;
  assigned: boolean;
};
