import './App.css';

import React from 'react';
import { useStore } from './Store';
import Table from './Table';
import Editor from './Editor';


const App = () => {
  const { state, selectEmployee } = useStore();
  return (
    <>
      <div className={['app', state.selectedEmployee ? 'has-editor' : ''].join(' ')}>
        <div className="header">
          <h1>Plexxis Employees</h1>
          <button className="pure-button pure-button-primary" onClick={() => { selectEmployee('new') }}>New Employee</button>
        </div>
        <div className="table-container">
          <Table />
        </div>
        <div className="editor-container">
          <Editor />
        </div>
      </div>
      {state.selectedEmployee && <div className="overlay" />}
    </>
  );
};

export default App;
