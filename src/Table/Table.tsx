import "react-table/react-table.css";
import "./styles.css";

import React, { useEffect, useMemo } from 'react';
import ReactTable, { Column, RowInfo, TableCellRenderer } from 'react-table';
import { Employee } from '../../types';
import { useStore } from '../Store';
import { toHexColour } from '../utils';

const ActionsCell: TableCellRenderer = ({ original }) => {
  const { selectEmployee, deleteEmployee } = useStore();
  const handleEdit = () => selectEmployee(original.id);
  const handleDelete = () => deleteEmployee(original.id);
  return (
    <>
      <i onClick={handleEdit} className="material-icons btn">edit</i>
      <i onClick={handleDelete} className="material-icons btn">delete</i>
    </>
  )
};

const StartColour: TableCellRenderer = ({ value }) => {
  const color = useMemo(() => toHexColour(value), [value]);
  return (
    <i className="material-icons" style={{ color }} >star</i>
  );
};

const columns: Column<Employee>[] = [
  {
    accessor: 'color',
    maxWidth: 40,
    Cell: StartColour
  },
  { Header: 'Name', accessor: 'name' },
  { Header: 'Code', accessor: 'code', maxWidth: 80 },
  { Header: 'Profession', accessor: 'profession' },
  { Header: 'City', accessor: 'city' },
  { Header: 'Branch', accessor: 'branch' },
  {
    Header: 'Assigned',
    accessor: 'assigned',
    maxWidth: 80,
    style: {
      alignItems: 'center',
      display: 'flex',
      justifyContent: 'center'
    },
    Cell: ({ value }) => <i className="material-icons">{value ? 'check_box' : 'check_box_outline_blank'}</i>
  },
  {
    id: '__actions',
    Header: '',
    accessor: () => 'X',
    filterable: false,
    sortable: false,
    resizable: false,
    maxWidth: 90,
    Cell: ActionsCell,
    className: 'action-cell'
  },
];

const Table = () => {
  const { fetchEmployees, selectEmployee, state } = useStore();
  useEffect(() => {
    fetchEmployees();
  }, []);

  if (!state.employees.length) return null;
  return (
    <ReactTable<Employee>
      className="-highlight"
      data={state.employees}
      columns={columns}
      showPagination={false}
      pageSize={state.employees.length}
      getTrProps={(_: any, rowInfo: RowInfo | undefined) => ({
        className: rowInfo && state.selectedEmployee && rowInfo.original === state.selectedEmployee ? 'selected' : ''
      })}
    />
  )
};

export default Table;
