import './styles.css';

import React from 'react';
import { Formik, FormikConfig } from 'formik';
import { Employee } from '../../types';
import { useStore } from '../Store';
import { toHexColour } from '../utils';

const Editor = () => {
  const { state, selectEmployee, insertEmployee, updateEmployee } = useStore();

  if (!state.selectedEmployee) return null;
  const handleSubmit: FormikConfig<Employee>['onSubmit'] = async (values, { setSubmitting, resetForm }) => {
    setSubmitting(true);
    if (values.id) await updateEmployee(values);
    if (!values.id) await insertEmployee(values);
    setSubmitting(false);
    resetForm();
    selectEmployee();
  }
  return (
    <div className="editor-box">
      <Formik<Employee>
        initialValues={state.selectedEmployee}
        onSubmit={handleSubmit}
      >
        {({ handleSubmit, handleChange, handleReset, values, isSubmitting }) => (
          <form className="pure-form" onSubmit={handleSubmit} onReset={handleReset}>
            <label htmlFor="name">Name</label>
            <input type="text" name="name" value={values.name} onChange={handleChange} />

            <label htmlFor="code">Code</label>
            <input type="text" name="code" value={values.code} onChange={handleChange} />

            <label htmlFor="profession">Profession</label>
            <input type="text" name="profession" value={values.profession} onChange={handleChange} />

            <label htmlFor="color">Color</label>
            <input type="color" name="color" value={toHexColour(values.color)} onChange={handleChange} />

            <label htmlFor="city">City</label>
            <input type="text" name="city" value={values.city} onChange={handleChange} />

            <label htmlFor="branch">Branch</label>
            <input type="text" name="branch" value={values.branch} onChange={handleChange} />

            <label htmlFor="assigned" className="pure-checkbox">
              <input type="checkbox" name="assigned" value={1} onChange={handleChange} /> Assigned
            </label>
            <div className="buttons">
              <button disabled={isSubmitting} className="pure-button pure-button-primary" type="submit">Save</button>
              <button disabled={isSubmitting} className="pure-button" type="reset">Reset</button>
              <button disabled={isSubmitting} onClick={() => { handleReset(); selectEmployee() }} className="pure-button" type="button">Close</button>
            </div>
          </form>
        )}
      </Formik>
    </div>
  );
};

export default Editor;
