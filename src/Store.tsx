import React, { createContext, useContext, useReducer, Dispatch } from 'react';
import { Employee } from '../types';

type State = {
  employees: Employee[];
  selectedEmployee: Employee | null;
};

const API_URL = 'http://localhost:3001/api';

type Methods = 'get' | 'post' | 'put' | 'delete';
type ApiFetch = (input: string, opt?: RequestInit) => Promise<Response>;

const api = {} as Record<Methods, ApiFetch>
for (const method of ['get', 'post', 'put', 'delete'] as Methods[]) {
  (api)[method] = (input, opt = {}) => fetch(`${API_URL}${input}`, {
    method,
    headers: { "Content-Type": "application/json" },
    ...opt
  });
}

const DEFAULT_EMP = {} as Employee;

const createActions = (dispatch: Dispatch<Partial<State>>, state: State) => ({
  async fetchEmployees() {
    const response = await api.get(`/employees`);
    const employees = await response.json();
    dispatch({ employees });
  },
  selectEmployee(id?: number | 'new') {
    if (!id) return dispatch({ selectedEmployee: null });
    if (id === 'new') return dispatch({ selectedEmployee: DEFAULT_EMP })
    const selectedEmployee = state.employees.find((e) => e.id === id);
    dispatch({ selectedEmployee });
  },
  async updateEmployee(e: Employee) {
    await api.put(`/employees/${e.id}`, { body: JSON.stringify(e) });
    const { employees, selectedEmployee } = state;
    const idx = employees.findIndex((e) => e === selectedEmployee);
    dispatch({ employees: Object.assign([], employees, { [idx]: e }), selectedEmployee: e });
  },
  async insertEmployee(e: Omit<Employee, 'id'>) {
    const resp = await api.post(`/employees`, { body: JSON.stringify(e) });
    const newEmp = await resp.json();
    const employees = [...state.employees, newEmp];
    dispatch({ employees, selectedEmployee: newEmp });
  },
  async deleteEmployee(id: number) {
    await api.delete(`/employees/${id}`);
    const employees = state.employees.filter((e) => e.id !== id);

    dispatch({ employees, selectedEmployee: null });
  }
});

type Ctx = ReturnType<typeof createActions> & { state: State; };

const reducer = (s: State, a: Partial<State>) => ({ ...s, ...a });

const StoreCtx = createContext<Ctx | null>(null);

export const Provider = ({ children }: { children: React.ReactNode }) => {
  const [state, dispatch] = useReducer<State, Partial<State>>(reducer, { employees: [], selectedEmployee: null });
  const actions = createActions(dispatch, state);
  return (
    <StoreCtx.Provider value={{ state, ...actions }}>
      {children}
    </StoreCtx.Provider>
  );
};

export const useStore = () => {
  const ctx = useContext(StoreCtx);
  if (!ctx) throw new Error('Must be wraped in provider');
  return ctx;
};
