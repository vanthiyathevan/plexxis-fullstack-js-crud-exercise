const cvs = document.createElement('canvas');
cvs.height = 1;
cvs.width = 1;

/**
 * Takes any colour value and gives a hex version of it.
 * This ignores alpha channel
 * @param colour 
 */
export const toHexColour = (colour: string) => {
  const ctx = cvs.getContext('2d')!;
  ctx.fillStyle = colour;
  ctx.fillRect(0, 0, 1, 1);
  const [r, g, b] = Array.from(ctx.getImageData(0, 0, 1, 1).data)
    .map((c) => `0${c.toString(16)}`.slice(-2));
  ctx.clearRect(0, 0, 1, 1);
  return `#${r}${g}${b}`;
}
